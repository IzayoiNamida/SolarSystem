attribute   vec4 vPosition;
attribute   vec4 vNormal;
attribute   vec4 vColor;
attribute vec2 vuv;
// output values that will be interpretated per-fragment
varying  vec3 fN;
varying  vec3 fE;
varying  vec3 fL;
varying  vec3 fL2;

uniform mat4 ModelView;
uniform vec4 LightPosition;
uniform vec4 LightPosition2;
uniform mat4 Projection;
uniform vec3 theta;
uniform vec3 AdamTheta, EarthTheta, HesperTheta;

varying vec4 color;
varying vec2 uv;

varying float intensity;

void main()
{
    color = vColor;
    uv = vuv;
    //
    vec3 angles = radians( theta );
    vec3 c = cos( angles );
    vec3 s = sin( angles );
    
    // Remeber: thse matrices are column-major
    mat4 rx = mat4( 1.0,  0.0,  0.0, 0.0,
                   0.0,  c.x,  s.x, 0.0,
                   0.0, -s.x,  c.x, 0.0,
                   0.0,  0.0,  0.0, 1.0 );
    
    mat4 ry = mat4( c.y, 0.0, -s.y, 0.0,
                   0.0, 1.0,  0.0, 0.0,
                   s.y, 0.0,  c.y, 0.0,
                   0.0, 0.0,  0.0, 1.0 );
    
    // Workaround for bug in ATI driver
    ry[1][0] = 0.0;
    ry[1][1] = 1.0;
    
    mat4 rz = mat4( c.z, -s.z, 0.0, 0.0,
                   s.z,  c.z, 0.0, 0.0,
                   0.0,  0.0, 1.0, 0.0,
                   0.0,  0.0, 0.0, 1.0 );
    
    // Workaround for bug in ATI driver
    rz[2][2] = 1.0;
    vec4 temp =  vPosition;
    vec4 tempNormal = vNormal;
    
    
    //
    fN = tempNormal.xyz;
    fE = temp.xyz;
    fL = (ModelView*LightPosition).xyz;
    fL2 = (ModelView*LightPosition2).xyz;
    
    if( LightPosition.w != 0.0 ) {
        fL = LightPosition.xyz - temp.xyz;
    }
    if( LightPosition2.w != 0.0 ) {
        fL2 = LightPosition2.xyz - temp.xyz;
    }
    //fN = vNormal;
    //fE = vPosition.xyz;
    //fL = (ModelView*LightPosition).xyz;
    
    if( LightPosition.w != 0.0 ) {
        fL = LightPosition.xyz - vPosition.xyz;
    }
    if( LightPosition2.w != 0.0 ) {
        fL2 = LightPosition2.xyz - vPosition.xyz;
    }

    vec4 lightDir = normalize(LightPosition);
    intensity = dot(lightDir,vNormal);
    
    
    gl_Position = Projection*ModelView * vPosition;
    
}
