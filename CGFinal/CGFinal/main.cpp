//
//  main.cpp
//  CGFinal
//
//  Created by izayoi on 2018/1/9.
//  Copyright © 2018年 izayoi. All rights reserved.
//

#include <iostream>
#include <GLUT/GLUT.h>
#include <OpenGL/OpenGL.h>
#include "include/Angel.h"
#include "ImageLoader.h"

using namespace std;
typedef Angel::vec4 point4;
typedef Angel::vec4 color4;

const int NumTimesToSubdivide = 5;
const int NumTriangles        = 4096;  // (4 faces)^(NumTimesToSubdivide + 1)
const int NumVertices         = 3 * NumTriangles;
GLuint program,program_adam;
GLuint buffer;
GLuint skybox[5];

GLfloat Left = -2.0, Right = 2.0;
GLfloat top = 2.0, bottom = -2.0;
GLfloat zNear = -20.0, zFar = 20.0;
//---------------------------------------------------------------------------

point4 points[NumVertices];
vec4   normals[NumVertices];

color4 colors1[NumVertices];
color4 colors2[NumVertices];
color4 colors3[NumVertices];
color4 colors4[NumVertices];
color4 colors5[NumVertices];
color4 colors6[NumVertices];
color4 colors7[NumVertices];
color4 colors8[NumVertices];
color4 colors9[NumVertices];
color4 colors10[NumVertices];

// Model-view and projection matrices uniform location
GLuint  ModelView, Projection;

enum { Xaxis = 0, Yaxis = 1, Zaxis = 2, NumAxes = 3 };
int      Axis = Yaxis;
GLfloat  Theta[NumAxes] = { 0.0, 0.0, 0.0 };
GLfloat  Theta_adam[3] = { 0.0, 0.0, 0.0 };

GLuint  theta;
GLuint AdamTheta, EarthTheta, HesperTheta;
GLuint vuv;
//the index of array points and normals
int Index = 0;

point4 light_position( 0.0, 2.0, 0.0, 0.0 );
point4 light_position2( 0.0, -2.0, 0.0, 0.0 );
color4 light_ambient( 0.2, 0.2, 0.2, 1.0 );
color4 light_diffuse( 1.0, 1.0, 1.0, 1.0 );
color4 light_specular( 1.0, 1.0, 1.0, 1.0 );

color4 material_ambient( 1.0, 0.0, 1.0, 1.0 );
color4 material_diffuse( 1.0, 0.8, 0.0, 1.0 );
color4 material_specular( 1.0, 0.0, 1.0, 1.0 );
float  material_shininess = 5.0;

color4 ambient_product = light_ambient * material_ambient;
color4 diffuse_product = light_diffuse * material_diffuse;
color4 specular_product = light_specular * material_specular;

color4 vertex_colors[10] = {
    color4( 1.0, 0.0, 0.0, 1.0 ),  // sun
    color4( 1.3, 1.0, 0.2, 1.0 ),  // adam
    color4( 1.0, 1.0, 0.0, 1.0 ),  // hesper
    color4( 0.1, 0.2, 0.6, 1.0 ),  // earth
    color4( 1.0, 0.0, 0.1, 1.0 ),  // mars
    color4( 0.8, 0.7, 0.0, 1.0 ),  // jupiter
    color4( 0.6, 0.6, 0.0, 1.0 ),  // saturn
    color4( 0.3, 0.3, 0.7, 1.0 ),  // uranus
    color4( 0.0, 0.1, 1.0, 1.0 )   // neptune
};

//将分好的三角形按顺序存储到数组中,在glDrawArrays画出
void triangle( const point4& a, const point4& b, const point4& c )
{
    vec4  normal = vec4(normalize( cross(b - a, c - b) ),1.0);
    
    normals[Index] = normal;  points[Index] = a;  colors1[Index] = vertex_colors[0];
    colors2[Index] = vertex_colors[1]; colors3[Index] = vertex_colors[2];
    colors4[Index] = vertex_colors[3]; colors5[Index] = vertex_colors[4];
    colors6[Index] = vertex_colors[5]; colors7[Index] = vertex_colors[6];
    colors8[Index] = vertex_colors[7]; colors9[Index] = vertex_colors[8];
    colors10[Index] = vertex_colors[9]; Index++;
    normals[Index] = normal;  points[Index] = b;  colors1[Index] = vertex_colors[0];
    colors2[Index] = vertex_colors[1]; colors3[Index] = vertex_colors[2];
    colors4[Index] = vertex_colors[3]; colors5[Index] = vertex_colors[4];
    colors6[Index] = vertex_colors[5]; colors7[Index] = vertex_colors[6];
    colors8[Index] = vertex_colors[7]; colors9[Index] = vertex_colors[8];
    colors10[Index] = vertex_colors[9]; Index++;
    normals[Index] = normal;  points[Index] = c;  colors1[Index] = vertex_colors[0];
    colors2[Index] = vertex_colors[1]; colors3[Index] = vertex_colors[2];
    colors4[Index] = vertex_colors[3]; colors5[Index] = vertex_colors[4];
    colors6[Index] = vertex_colors[5]; colors7[Index] = vertex_colors[6];
    colors8[Index] = vertex_colors[7]; colors9[Index] = vertex_colors[8];
    colors10[Index] = vertex_colors[9]; Index++;
}

//----------------------------------------------------------------------------

//find out the dot product of two vector
point4 unit( const point4& p )
{
    float len = p.x*p.x + p.y*p.y + p.z*p.z;
    
    point4 t;
    if ( len > DivideByZeroTolerance ) {
        t = p / sqrt(len);
        t.w = 1.0;
    }
    
    return t;
}

//以每个面为基础，以递归形式在每个面上继续分三角形
void divide_triangle( const point4& a, const point4& b,
                     const point4& c, int count )
{
    if ( count > 0 ) {
        point4 v1 = unit( a + b );
        point4 v2 = unit( a + c );
        point4 v3 = unit( b + c );
        divide_triangle(  a, v1, v2, count - 1 );
        divide_triangle(  c, v2, v3, count - 1 );
        divide_triangle(  b, v3, v1, count - 1 );
        divide_triangle( v1, v3, v2, count - 1 );
    }
    else {
        triangle( a, b, c );
    }
}

//this function define a tetrahedron in 3D and use it to get a sphere
void tetrahedron( int count )
{
    point4 v[4] = {
        vec4( 0.0, 0.0, 1.0, 1.0 ),
        vec4( 0.0, 0.942809, -0.333333, 1.0 ),
        vec4( -0.816497, -0.471405, -0.333333, 1.0 ),
        vec4( 0.816497, -0.471405, -0.333333, 1.0 )
    }; //this make a tetrahedron in 3D
    
    divide_triangle( v[0], v[1], v[2], count );//the 1st surface and so on
    divide_triangle( v[3], v[2], v[1], count );
    divide_triangle( v[0], v[3], v[1], count );
    divide_triangle( v[0], v[2], v[3], count );
}

//----------------------------------------------------------------------

static float year = 0, day = 0,adamYear=0,hesperYear=0,marsYear=0,jupiterYear=0,saturnYear=0,uranusYear=0,neptuneYear=0,sunYear=0,month=0;
static float mar_satellite_1,mar_satellite_2 ;
float light_angle=0;
float light_radius=8.0;
double x=0,y=0;

//----------------------------------------------------------------------

void sphere()
{
    tetrahedron( NumTimesToSubdivide );
}

point4 at( 0.0, 0.0, 0.0, 1.0 );
point4 eye( 0.0, 0.0, 14.0, 1.0 );
vec4   up( 0.0, 1.0, 0.0, 0.0 );

mat4 model_view = LookAt(eye, at, up);
float asca = 0;
void sun()
{
    mat4 instance = Scale( 0.65,0.65,0.65 ) ;
    glUniformMatrix4fv( ModelView, 1, GL_TRUE, model_view * instance *RotateY(day) );
    glDrawArrays( GL_TRIANGLES, 0, NumVertices );
}

void adam()
{
    
    
    mat4 instance =  Scale( 0.15,0.15,0.15 )*RotateY(day)  * RotateY(adamYear)*  Translate(-5.5, 0.0, 0.0) ;
    glUniformMatrix4fv( ModelView, 1, GL_TRUE, model_view * instance );
    glDrawArrays( GL_TRIANGLES, 0, NumVertices );
}

void hesper()
{
    mat4 instance = Scale( 0.2,0.2,0.2 ) * RotateY(day) * RotateY(hesperYear) * Translate(-6.6, 0.0, 0.0);
    glUniformMatrix4fv( ModelView, 1, GL_TRUE, model_view * instance );
    glDrawArrays( GL_TRIANGLES, 0, NumVertices );
}

void earth()
{
    mat4 instance = Scale( 0.3,0.3,0.3 ) * RotateY(day) * RotateY(year) * Translate(-6.6, 0.0, 0.0);
    glUniformMatrix4fv( ModelView, 1, GL_TRUE, model_view * instance );
    glDrawArrays( GL_TRIANGLES, 0, NumVertices );
}

void moon()
{
    mat4 instance = Scale( 0.15,0.15,0.15 ) * RotateY(month)* Translate(-1.1, 0.0, 0.0)* RotateY(day) * RotateY(year) * Translate(-6.6, 0.0, 0.0);
    glUniformMatrix4fv( ModelView, 1, GL_TRUE, model_view * instance );
    glDrawArrays( GL_TRIANGLES, 0, NumVertices );
}

void mars()
{
    mat4 instance = Scale( 0.3,0.3,0.3 ) * RotateY(day) * RotateY(marsYear) * Translate(-9.0, 0.0, 0.0);
    glUniformMatrix4fv( ModelView, 1, GL_TRUE, model_view * instance );
    glDrawArrays( GL_TRIANGLES, 0, NumVertices );
}

void jupiter()
{
    mat4 instance = Scale( 0.6,0.6,0.6 ) * RotateY(day) * RotateY(jupiterYear) * Translate(-7.0, 0.0, 0.0);
    glUniformMatrix4fv( ModelView, 1, GL_TRUE, model_view * instance );
    glDrawArrays( GL_TRIANGLES, 0, NumVertices );
}

void saturn()
{
    mat4 instance = Scale( 0.4,0.4,0.4 ) * RotateY(day) * RotateY(saturnYear) * Translate(-14.0, 0.0, 0.0);
    glUniformMatrix4fv( ModelView, 1, GL_TRUE, model_view * instance );
    glDrawArrays( GL_TRIANGLES, 0, NumVertices );
}

void uranus()
{
    mat4 instance = Scale( 0.3,0.3,0.3 ) * RotateY(day) * RotateY(uranusYear) * Translate(-22.0, 0.0, 0.0);
    glUniformMatrix4fv( ModelView, 1, GL_TRUE, model_view * instance );
    glDrawArrays( GL_TRIANGLES, 0, NumVertices );
}

void neptune()
{
    mat4 instance = Scale( 0.3,0.3,0.3 ) * RotateY(day) * RotateY(neptuneYear) * Translate(-28.0, 0.0, 0.0);
    glUniformMatrix4fv( ModelView, 1, GL_TRUE, model_view * instance );
    glDrawArrays( GL_TRIANGLES, 0, NumVertices );
}

vec4 skypoints[5][6];
vec4 skynormals[5][6];
vec4 skycolors [5][6];
vec2 skyuv[5][6];

void initSkybox(){
    for(int i = 0;i<5;i++){
        skypoints[i][0]=vec4(-10,0,10,1);
        skypoints[i][1]=vec4(-10,0,-10,1);
        skypoints[i][2]=vec4(10,0,10,1);
        skypoints[i][3]=vec4(10,0,10,1);
        skypoints[i][4]=vec4(-10,0,-10,1);
        skypoints[i][5]=vec4(10,0,-10,1);
        
        skyuv[i][0]=vec2(0,1);
        skyuv[i][1]=vec2(0,0);
        skyuv[i][2]=vec2(1,1);
        skyuv[i][3]=vec2(1,1);
        skyuv[i][4]=vec2(0,0);
        skyuv[i][5]=vec2(1,0);

        for(int j = 0;j<6;j++){
            skynormals[i][j] = vec4(0,1,0,0);
            skycolors[i][j]=vec4(1,1,1,1);
        }
    }
}
GLuint vPosition;
GLuint vNormal;
GLuint vColor;
Angel::mat4 projection;

NeroEngine::GLTexture sky_left;
NeroEngine::GLTexture sky_right;
NeroEngine::GLTexture sky_front;
NeroEngine::GLTexture sky_back;
NeroEngine::GLTexture sky_top;

GLuint texture;

NeroEngine::ImageLoader skyboxImageLoader;

void init()
{
    initSkybox();
    sphere();
    
    sky_top = skyboxImageLoader.loadPNG("/Users/izayoi/Documents/复习资料/大三/计算机图形学/homework/CGFinal/CGFinal/skyox/top.png");
    sky_left = skyboxImageLoader.loadPNG("/Users/izayoi/Documents/复习资料/大三/计算机图形学/homework/CGFinal/CGFinal/skyox/left.png");
    sky_right = skyboxImageLoader.loadPNG("/Users/izayoi/Documents/复习资料/大三/计算机图形学/homework/CGFinal/CGFinal/skyox/right.png");
    sky_front = skyboxImageLoader.loadPNG("/Users/izayoi/Documents/复习资料/大三/计算机图形学/homework/CGFinal/CGFinal/skyox/front.png");
    sky_back = skyboxImageLoader.loadPNG("/Users/izayoi/Documents/复习资料/大三/计算机图形学/homework/CGFinal/CGFinal/skyox/back.png");
    
    
    GLuint vao;
    glGenVertexArraysAPPLE( 1 , &vao );
    glBindVertexArrayAPPLE( vao );
    
    
    //fucking solars buffer
    glGenBuffers( 1, &buffer );
    glBindBuffer( GL_ARRAY_BUFFER, buffer );
    glBufferData( GL_ARRAY_BUFFER, sizeof(points) + sizeof(normals) + sizeof(colors1)*10,
                 NULL, GL_DYNAMIC_DRAW );
    glBufferSubData( GL_ARRAY_BUFFER, 0, sizeof(points), points );
    glBufferSubData( GL_ARRAY_BUFFER, sizeof(points),
                    sizeof(normals), normals );
    glBufferSubData( GL_ARRAY_BUFFER, sizeof(points)+sizeof(normals),
                    sizeof(colors1), colors1 );
    glBufferSubData( GL_ARRAY_BUFFER, sizeof(points)+sizeof(normals) + sizeof(colors1),
                    sizeof(colors2), colors2 );
    glBufferSubData( GL_ARRAY_BUFFER, sizeof(points)+sizeof(normals) + sizeof(colors1)*2,
                    sizeof(colors3), colors4 );
    glBufferSubData( GL_ARRAY_BUFFER, sizeof(points)+sizeof(normals) + sizeof(colors1)*3,
                    sizeof(colors3), colors5 );
    glBufferSubData( GL_ARRAY_BUFFER, sizeof(points)+sizeof(normals) + sizeof(colors1)*4,
                    sizeof(colors3), colors6 );
    glBufferSubData( GL_ARRAY_BUFFER, sizeof(points)+sizeof(normals) + sizeof(colors1)*5,
                    sizeof(colors3), colors7 );
    glBufferSubData( GL_ARRAY_BUFFER, sizeof(points)+sizeof(normals) + sizeof(colors1)*6,
                    sizeof(colors3), colors8 );
    glBufferSubData( GL_ARRAY_BUFFER, sizeof(points)+sizeof(normals) + sizeof(colors1)*7,
                    sizeof(colors3), colors9 );
    //skybox buffer
    
    for(int i =0 ; i<6;i++){
        glGenBuffers(1, &skybox[i]);
        glBindBuffer(GL_ARRAY_BUFFER, skybox[i]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(skypoints[i])+sizeof(skynormals[i])+sizeof(skycolors[i])+sizeof(skyuv[i]), NULL ,GL_DYNAMIC_DRAW);
        glBufferSubData( GL_ARRAY_BUFFER, 0, sizeof(skypoints[i]), skypoints[i] );
        glBufferSubData( GL_ARRAY_BUFFER, sizeof(skypoints[i]),
                        sizeof(skynormals[i]), skynormals[i] );
        glBufferSubData( GL_ARRAY_BUFFER, sizeof(skypoints[i])+sizeof(skynormals[i]),
                        sizeof(skycolors[i]), skycolors[i] );
        glBufferSubData( GL_ARRAY_BUFFER, sizeof(skypoints[i])+sizeof(skynormals[i])+sizeof(skycolors[i]),
                        sizeof(skyuv[i]), skyuv[i] );
    }

    
    
    program = InitShader("/Users/izayoi/Documents/复习资料/大三/计算机图形学/homework/CGFinal/CGFinal/vshader56.glsl",
                         "/Users/izayoi/Documents/复习资料/大三/计算机图形学/homework/CGFinal/CGFinal/fshader56.glsl");
    

    vPosition = glGetAttribLocation( program, "vPosition" );
    vNormal = glGetAttribLocation( program, "vNormal" );
    vColor = glGetAttribLocation( program, "vColor" );
    vuv = glGetAttribLocation(program, "vuv");
    
    
    glUniform4fv( glGetUniformLocation(program, "AmbientProduct"),1, ambient_product );
    glUniform4fv( glGetUniformLocation(program, "DiffuseProduct"),1, diffuse_product );
    glUniform4fv( glGetUniformLocation(program, "SpecularProduct"),1, specular_product );
    glUniform4fv( glGetUniformLocation(program, "LightPosition"),1, light_position );
    glUniform4fv( glGetUniformLocation(program, "LightPosition2"),1, light_position2 );
    glUniform1f( glGetUniformLocation(program, "Shininess"),material_shininess );
    
    ModelView = glGetUniformLocation( program, "ModelView" );
    Projection = glGetUniformLocation( program, "Projection" );
    theta = glGetUniformLocation( program, "theta" );
    
    texture = glGetUniformLocation(program, "MyTexture");
    
    glEnable( GL_DEPTH_TEST );
    glClearColor( 1.0, 1.0, 1.0, 1.0 );
}

float  theta1 = 0;

void display()
{
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    // skybox
    //left
    glUseProgram( program );
    glActiveTexture(GL_TEXTURE0);
    glUniform1i(texture, 0);
    glBindTexture(GL_TEXTURE_2D, sky_left.id);
    glUniformMatrix4fv( Projection, 1, GL_TRUE, projection );
    glUniformMatrix4fv(ModelView, 1, GL_TRUE, model_view*RotateX(90)*Translate(0, -10, 0));
    glBindBuffer(GL_ARRAY_BUFFER, skybox[0]);
    glEnableVertexAttribArray( vPosition);
    glEnableVertexAttribArray( vNormal );
    glEnableVertexAttribArray( vColor );
    glEnableVertexAttribArray( vuv );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(0) );
    glVertexAttribPointer( vNormal, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(skypoints[0])) );
    glVertexAttribPointer( vColor, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(skypoints[0])+sizeof(skynormals[0])) );
    glVertexAttribPointer( vuv, 2, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(skypoints[0])+sizeof(skynormals[0])+sizeof(skycolors[0])) );
    glDrawArrays(GL_TRIANGLES,0, 6);
    glDisableVertexAttribArray(vuv);
    glDisableVertexAttribArray(vColor);
    glDisableVertexAttribArray(vNormal);
    glDisableVertexAttribArray(vPosition);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glLinkProgram(0);
    //right
    glUseProgram( program );
    glUniformMatrix4fv( Projection, 1, GL_TRUE, projection );
    glUniformMatrix4fv(ModelView, 1, GL_TRUE, model_view);
    glBindBuffer(GL_ARRAY_BUFFER, skybox[1]);
    glEnableVertexAttribArray( vPosition);
    glEnableVertexAttribArray( vNormal );
    glEnableVertexAttribArray( vColor );
    glEnableVertexAttribArray( vuv );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(0) );
    glVertexAttribPointer( vNormal, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(skypoints[1])) );
    glVertexAttribPointer( vColor, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(skypoints[1])+sizeof(skynormals[1])) );
    glVertexAttribPointer( vuv, 2, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(skypoints[1])+sizeof(skynormals[1])+sizeof(skycolors[1])) );
    glDrawArrays(GL_TRIANGLES,0, 6);
    glDisableVertexAttribArray(vuv);
    glDisableVertexAttribArray(vColor);
    glDisableVertexAttribArray(vNormal);
    glDisableVertexAttribArray(vPosition);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glLinkProgram(0);
    
    
    glUseProgram( program );
    glUniformMatrix4fv( Projection, 1, GL_TRUE, projection );
    glUniformMatrix4fv(ModelView, 1, GL_TRUE, model_view);
    glBindBuffer(GL_ARRAY_BUFFER, skybox[2]);
    glEnableVertexAttribArray( vPosition);
    glEnableVertexAttribArray( vNormal );
    glEnableVertexAttribArray( vColor );
    glEnableVertexAttribArray( vuv );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(0) );
    glVertexAttribPointer( vNormal, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(skypoints[2])) );
    glVertexAttribPointer( vColor, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(skypoints[2])+sizeof(skynormals[2])) );
    glVertexAttribPointer( vuv, 2, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(skypoints[2])+sizeof(skynormals[2])+sizeof(skycolors[2])) );
    glDrawArrays(GL_TRIANGLES,0, 6);
    glDisableVertexAttribArray(vuv);
    glDisableVertexAttribArray(vColor);
    glDisableVertexAttribArray(vNormal);
    glDisableVertexAttribArray(vPosition);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glLinkProgram(0);
    
    glUseProgram( program );
    glUniformMatrix4fv( Projection, 1, GL_TRUE, projection );
    glUniformMatrix4fv(ModelView, 1, GL_TRUE, model_view);
    glBindBuffer(GL_ARRAY_BUFFER, skybox[3]);
    glEnableVertexAttribArray( vPosition);
    glEnableVertexAttribArray( vNormal );
    glEnableVertexAttribArray( vColor );
    glEnableVertexAttribArray( vuv );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(0) );
    glVertexAttribPointer( vNormal, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(skypoints[3])) );
    glVertexAttribPointer( vColor, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(skypoints[3])+sizeof(skynormals[3])) );
    glVertexAttribPointer( vuv, 2, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(skypoints[3])+sizeof(skynormals[3])+sizeof(skycolors[3])) );
    glDrawArrays(GL_TRIANGLES,0, 6);
    glDisableVertexAttribArray(vuv);
    glDisableVertexAttribArray(vColor);
    glDisableVertexAttribArray(vNormal);
    glDisableVertexAttribArray(vPosition);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glLinkProgram(0);
    
    glUseProgram( program );
    glUniformMatrix4fv( Projection, 1, GL_TRUE, projection );
    glUniformMatrix4fv(ModelView, 1, GL_TRUE, model_view);
    glBindBuffer(GL_ARRAY_BUFFER, skybox[4]);
    glEnableVertexAttribArray( vPosition);
    glEnableVertexAttribArray( vNormal );
    glEnableVertexAttribArray( vColor );
    glEnableVertexAttribArray( vuv );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(0) );
    glVertexAttribPointer( vNormal, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(skypoints[4])) );
    glVertexAttribPointer( vColor, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(skypoints[4])+sizeof(skynormals[4])) );
    glVertexAttribPointer( vuv, 2, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(skypoints[4])+sizeof(skynormals[4])+sizeof(skycolors[4])) );
    glDrawArrays(GL_TRIANGLES,0, 6);
    glDisableVertexAttribArray(vuv);
    glDisableVertexAttribArray(vColor);
    glDisableVertexAttribArray(vNormal);
    glDisableVertexAttribArray(vPosition);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glLinkProgram(0);
    
    
    glUseProgram( program );
    glUniformMatrix4fv( Projection, 1, GL_TRUE, projection );
    glUniformMatrix4fv(ModelView, 1, GL_TRUE, model_view);
    glBindBuffer(GL_ARRAY_BUFFER, skybox[5]);
    glEnableVertexAttribArray( vPosition);
    glEnableVertexAttribArray( vNormal );
    glEnableVertexAttribArray( vColor );
    glEnableVertexAttribArray( vuv );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(0) );
    glVertexAttribPointer( vNormal, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(skypoints[5])) );
    glVertexAttribPointer( vColor, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(skypoints[5])+sizeof(skynormals[5])) );
    glVertexAttribPointer( vuv, 2, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(skypoints[5])+sizeof(skynormals[5])+sizeof(skycolors[5])) );
    glDrawArrays(GL_TRIANGLES,0, 6);
    glDisableVertexAttribArray(vuv);
    glDisableVertexAttribArray(vColor);
    glDisableVertexAttribArray(vNormal);
    glDisableVertexAttribArray(vPosition);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glLinkProgram(0);
    
    
    /////////
    glUseProgram( program );
    glUniform3fv( theta, 1, Theta );
    glUniformMatrix4fv( Projection, 1, GL_TRUE, projection );
    glUniformMatrix4fv(ModelView, 1, GL_TRUE, model_view );
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glEnableVertexAttribArray( vPosition );
    glEnableVertexAttribArray( vNormal );
    glEnableVertexAttribArray( vColor );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(0) );
    glVertexAttribPointer( vNormal, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(points)) );
    glVertexAttribPointer( vColor, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(points)+sizeof(normals)) );
    sun();
    glDisableVertexAttribArray(vColor);
    glDisableVertexAttribArray(vNormal);
    glDisableVertexAttribArray(vPosition);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glLinkProgram(0);
    
    
    glUseProgram( program );
    glUniform3fv( theta, 1, Theta );
    glUniform3fv( AdamTheta, 1, Theta_adam );
    glUniformMatrix4fv( Projection, 1, GL_TRUE, projection );
    glUniformMatrix4fv(ModelView, 1, GL_TRUE, model_view);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glEnableVertexAttribArray( vPosition );
    glEnableVertexAttribArray( vNormal );
    glEnableVertexAttribArray( vColor );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(0) );
    glVertexAttribPointer( vNormal, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(points)) );
    glVertexAttribPointer( vColor, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(points)+sizeof(normals)+sizeof(colors1)) );
    adam();
    glDisableVertexAttribArray(vColor);
    glDisableVertexAttribArray(vNormal);
    glDisableVertexAttribArray(vPosition);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glLinkProgram(0);
   
    
    glUseProgram( program );
    glUniform3fv( theta, 1, Theta );
    glUniform3fv( AdamTheta, 1, Theta_adam );
    glUniformMatrix4fv( Projection, 1, GL_TRUE, projection );
    glUniformMatrix4fv(ModelView, 1, GL_TRUE, model_view);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glEnableVertexAttribArray( vPosition );
    glEnableVertexAttribArray( vNormal );
    glEnableVertexAttribArray( vColor );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(0) );
    glVertexAttribPointer( vNormal, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(points)) );
    glVertexAttribPointer( vColor, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(points)+sizeof(normals)+sizeof(colors1)*2) );
    hesper();
    glDisableVertexAttribArray(vColor);
    glDisableVertexAttribArray(vNormal);
    glDisableVertexAttribArray(vPosition);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glLinkProgram(0);


    glUseProgram( program );
    glUniform3fv( theta, 1, Theta );
    glUniform3fv( AdamTheta, 1, Theta_adam );
    glUniformMatrix4fv( Projection, 1, GL_TRUE, projection );
    glUniformMatrix4fv(ModelView, 1, GL_TRUE, model_view);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glEnableVertexAttribArray( vPosition );
    glEnableVertexAttribArray( vNormal );
    glEnableVertexAttribArray( vColor );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(0) );
    glVertexAttribPointer( vNormal, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(points)) );
    glVertexAttribPointer( vColor, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(points)+sizeof(normals)+sizeof(colors1)*3) );
    earth();
    glDisableVertexAttribArray(vColor);
    glDisableVertexAttribArray(vNormal);
    glDisableVertexAttribArray(vPosition);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glLinkProgram(0);
    
    glUseProgram( program );
    glUniform3fv( theta, 1, Theta );
    glUniform3fv( AdamTheta, 1, Theta_adam );
    glUniformMatrix4fv( Projection, 1, GL_TRUE, projection );
    glUniformMatrix4fv(ModelView, 1, GL_TRUE, model_view);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glEnableVertexAttribArray( vPosition );
    glEnableVertexAttribArray( vNormal );
    glEnableVertexAttribArray( vColor );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(0) );
    glVertexAttribPointer( vNormal, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(points)) );
    glVertexAttribPointer( vColor, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(points)+sizeof(normals)+sizeof(colors1)*3) );
    moon();
    glDisableVertexAttribArray(vColor);
    glDisableVertexAttribArray(vNormal);
    glDisableVertexAttribArray(vPosition);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glLinkProgram(0);
    
    glUseProgram( program );
    glUniform3fv( theta, 1, Theta );
    glUniform3fv( AdamTheta, 1, Theta_adam );
    glUniformMatrix4fv( Projection, 1, GL_TRUE, projection );
    glUniformMatrix4fv(ModelView, 1, GL_TRUE, model_view);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glEnableVertexAttribArray( vPosition );
    glEnableVertexAttribArray( vNormal );
    glEnableVertexAttribArray( vColor );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(0) );
    glVertexAttribPointer( vNormal, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(points)) );
    glVertexAttribPointer( vColor, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(points)+sizeof(normals)+sizeof(colors1)*4) );
    mars();
    glDisableVertexAttribArray(vColor);
    glDisableVertexAttribArray(vNormal);
    glDisableVertexAttribArray(vPosition);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glLinkProgram(0);
    
    glUseProgram( program );
    glUniform3fv( theta, 1, Theta );
    glUniform3fv( AdamTheta, 1, Theta_adam );
    glUniformMatrix4fv( Projection, 1, GL_TRUE, projection );
    glUniformMatrix4fv(ModelView, 1, GL_TRUE, model_view);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glEnableVertexAttribArray( vPosition );
    glEnableVertexAttribArray( vNormal );
    glEnableVertexAttribArray( vColor );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(0) );
    glVertexAttribPointer( vNormal, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(points)) );
    glVertexAttribPointer( vColor, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(points)+sizeof(normals)+sizeof(colors1)*5) );
    jupiter();
    glDisableVertexAttribArray(vColor);
    glDisableVertexAttribArray(vNormal);
    glDisableVertexAttribArray(vPosition);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glLinkProgram(0);
    
    glUseProgram( program );
    glUniform3fv( theta, 1, Theta );
    glUniform3fv( AdamTheta, 1, Theta_adam );
    glUniformMatrix4fv( Projection, 1, GL_TRUE, projection );
    glUniformMatrix4fv(ModelView, 1, GL_TRUE, model_view);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glEnableVertexAttribArray( vPosition );
    glEnableVertexAttribArray( vNormal );
    glEnableVertexAttribArray( vColor );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(0) );
    glVertexAttribPointer( vNormal, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(points)) );
    glVertexAttribPointer( vColor, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(points)+sizeof(normals)+sizeof(colors1)*6) );
    saturn();
    glDisableVertexAttribArray(vColor);
    glDisableVertexAttribArray(vNormal);
    glDisableVertexAttribArray(vPosition);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glLinkProgram(0);
    
    glUseProgram( program );
    glUniform3fv( theta, 1, Theta );
    glUniform3fv( AdamTheta, 1, Theta_adam );
    glUniformMatrix4fv( Projection, 1, GL_TRUE, projection );
    glUniformMatrix4fv(ModelView, 1, GL_TRUE, model_view);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glEnableVertexAttribArray( vPosition );
    glEnableVertexAttribArray( vNormal );
    glEnableVertexAttribArray( vColor );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(0) );
    glVertexAttribPointer( vNormal, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(points)) );
    glVertexAttribPointer( vColor, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(points)+sizeof(normals)+sizeof(colors1)*7) );
    uranus();
    glDisableVertexAttribArray(vColor);
    glDisableVertexAttribArray(vNormal);
    glDisableVertexAttribArray(vPosition);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glLinkProgram(0);
    
    glUseProgram( program );
    glUniform3fv( theta, 1, Theta );
    glUniform3fv( AdamTheta, 1, Theta_adam );
    glUniformMatrix4fv( Projection, 1, GL_TRUE, projection );
    glUniformMatrix4fv(ModelView, 1, GL_TRUE, model_view);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glEnableVertexAttribArray( vPosition );
    glEnableVertexAttribArray( vNormal );
    glEnableVertexAttribArray( vColor );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(0) );
    glVertexAttribPointer( vNormal, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(points)) );
    glVertexAttribPointer( vColor, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(sizeof(points)+sizeof(normals)+sizeof(colors1)*8) );
    neptune();
    glDisableVertexAttribArray(vColor);
    glDisableVertexAttribArray(vNormal);
    glDisableVertexAttribArray(vPosition);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glLinkProgram(0);
    
    glutSwapBuffers();
}

void mouse( int button, int state, int x, int y )
{
    
}

GLfloat add = 1.0;

void keyboard( unsigned char key, int x, int y )
{
    switch( key )
    {
        case 033: // Escape Key
        case 'q': case 'Q':
            exit( EXIT_SUCCESS );
            break;
        case 'w':
            cout << "w" <<endl;
            add += 0.5;
            break;
        case 'a':
            cout << "a" <<endl;
            add -= 0.5;
            break;
    }
}

void reshape(int width, int height)
{
    glViewport( 0, 0, width, height );//define the left-bottom corner postion of the graph.
    
    
    GLfloat aspect = GLfloat(width)/height;
    
    if ( aspect > 1.0 ) {
        Left *= aspect;
        Right *= aspect;
    }
    else {
        top /= aspect;
        bottom /= aspect;
    }
    projection = Perspective(45, 1, 0.1, 30);
    //projection = Ortho( Left, Right, bottom, top, zNear, zFar );//Orthogmal Matrix of the projection
    glUniformMatrix4fv( Projection, 1, GL_TRUE, projection );//Output the graph to the monitor

    
}

void myidle()
{
    day+=add;
    if (day>=360)
        day=day-360;
    //adamYear=0,hesperYear=0,marsYear=0,jupiterYear=0,saturnYear=0,uranusYear=0,neptuneYear=0;
    //  adamYear=(adamYear+1.2)%360;
    adamYear+=0.12;
    if(adamYear>=360)
        adamYear-=360;
    //   hesperYear=(hesperYear+2)%360;
    hesperYear+=0.10;
    if(hesperYear>=360)
        hesperYear-=360;
    //  year=(year+0.8)%360;
    year+=0.08;
    if(year>=360)
        year-=360;
    //   marsYear=(marsYear+0.6)%360;
    marsYear+=0.06;
    if(marsYear>=360)
        marsYear-=360;
    
    
    //   jupiterYear=(jupiterYear+0.5)%360;
    jupiterYear+=0.05;
    if(jupiterYear>=360)
        jupiterYear-=360;
    //   saturnYear=(saturnYear+0.4)%360;
    saturnYear+=0.04;
    if(saturnYear>=360)
        saturnYear-=360;
    //  uranusYear=(uranusYear+0.3)%360;
    uranusYear+=0.03;
    if(uranusYear>=360)
        uranusYear-=360;
    //   neptuneYear=(neptuneYear+0.1)%360;
    neptuneYear+=0.01;
    if(neptuneYear>=360)
        neptuneYear-=360;
    glutPostRedisplay();
    month+=0.03;
    if(month>=360)
        month-=360;
    
    glutPostRedisplay();
}

int main(int argc, char * argv[])
{
    // insert code here...
    glutInit( &argc, argv );
    glutInitDisplayMode( GLUT_RGBA | GLUT_DEPTH );
    glutInitWindowSize( 1024, 1024 );
    glutCreateWindow( "Izayoi Final Work" );
    
    init();
    
    glutDisplayFunc( display );
    glutReshapeFunc( reshape );
    glutIdleFunc(myidle);
    glutKeyboardFunc( keyboard );
    
    glutMainLoop();
    return 0;
}
