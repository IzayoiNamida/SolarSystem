
attribute   vec4 vPosition;
attribute   vec3 vNormal;

// output values that will be interpretated per-fragment

uniform mat4 ModelView;
uniform vec4 LightPosition;
uniform mat4 Projection;
uniform vec3 theta;
uniform vec3 AdamTheta;


void main()
{

    gl_Position = Projection * ModelView * vPosition;

}
