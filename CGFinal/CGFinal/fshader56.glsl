varying  vec3 fN;
varying  vec3 fL;
varying  vec3 fL2;
varying  vec3 fE;
varying  vec4 color;
varying vec2 uv;

uniform sampler2D MyTexture;

uniform vec4 AmbientProduct, DiffuseProduct, SpecularProduct;
uniform mat4 ModelView;
uniform vec4 LightPosition;
uniform float Shininess;

uniform vec3 lightDir;
varying float intensity;

void main()
{
    // Normalize the input lighting vectors
    vec3 N = normalize(fN);
    vec3 E = normalize(fE);
    vec3 L = normalize(fL);
    vec3 L2 = normalize(fL2);
    
    vec3 H = normalize( L + E );
    vec3 H2 = normalize( L2 + E );
    
    vec4 ambient = AmbientProduct;
    
    float Kd = max(dot(L, N), 0.0);
    float Kd2 = max(dot(L2, N), 0.0);
    vec4 diffuse = Kd*DiffuseProduct;
    vec4 diffuse2 = Kd2*DiffuseProduct;
    
    float Ks = pow(max(dot(N, H), 0.0), Shininess);
    float Ks2 = pow(max(dot(N, H2), 0.0), Shininess);
    vec4 specular = Ks*SpecularProduct;
    vec4 specular2 = Ks2*SpecularProduct;
    
    // discard the specular highlight if the light's behind the vertex
    if( dot(L, N) < 0.0 ) {
        specular = vec4(0.0, 0.0, 0.0, 1.0);
    }
    if( dot(L2, N) < 0.0 ) {
        specular2 = vec4(0.0, 0.0, 0.0, 1.0);
    }
    
    vec4 color1;
    
    if (intensity > 0.95)
        color1 = vec4(1.0,0.5,0.5,1.0);
    else if (intensity > 0.5)
        color1 = vec4(0.6,0.3,0.3,1.0);
    else if (intensity > 0.25)
        color1 = vec4(0.4,0.2,0.2,1.0);
    else
        color1 = vec4(0.2,0.1,0.1,1.0);
    
    gl_FragColor = ((ambient + diffuse + diffuse2 + specular + specular2) + color + color1) * texture2D(MyTexture,uv);
    
    gl_FragColor.a = 1.0;
}
